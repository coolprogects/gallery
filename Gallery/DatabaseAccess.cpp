#include "DatabaseAccess.h"


std::string DatabaseAccess::result = "";
int DatabaseAccess::amount = 0;

bool DatabaseAccess::open()
{
	std::string dbFileName = "galleryDB.sqlite";
	int doesFileExist = _access(dbFileName.c_str(), 0);
	int res = sqlite3_open(dbFileName.c_str(), &data_base_pointer);

	if (res != SQLITE_OK)
	{	
		data_base_pointer = nullptr;
		std::cout << "Failed to open DB" << std::endl;
		return -1;
	}

	if (doesFileExist == -1)
	{
		//using make_the_sql func for sql command. 
		make_the_sql("CREATE TABLE users (id INTEGER PRIMARY KEY, name TEXT);", 0);
		make_the_sql("CREATE TABLE albums(id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, name TEXT NOT NULL, creation_date TEXT NOT NULL, user_id INTEGER, FOREIGN KEY(user_id) REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE);", 0);
		make_the_sql("CREATE TABLE pictures(id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, name TEXT NOT NULL, creation_date TEXT NOT NULL, album_id INTEGER, location TEXT NOT NULL, FOREIGN KEY(album_id) REFERENCES albums(id) ON UPDATE CASCADE ON DELETE CASCADE);", 0);
		make_the_sql("CREATE TABLE tags(id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, picture_id INTEGER, user_id INTEGER, FOREIGN KEY(user_id) REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE, FOREIGN KEY(picture_id) REFERENCES pictures(id) ON UPDATE CASCADE ON DELETE CASCADE);", 0);
	}

	return true;
}

void DatabaseAccess::close()
{
	sqlite3_close(data_base_pointer);
	data_base_pointer = nullptr;
}

void DatabaseAccess::clear()
{
	//using make_the_sql func for sql command.
	make_the_sql("DELETE FROM users;", 0);
	make_the_sql("DELETE FROM albums;", 0);
	make_the_sql("DELETE FROM pictures;", 0);
	make_the_sql("DELETE FROM tags;", 0);
}


void DatabaseAccess::closeAlbum(Album& pAlbum)
{
}

void DatabaseAccess::deleteAlbum(const std::string& albumName, int userId)
{
	std::string my_command = "DELETE FROM pictures WHERE album_id = (SELECT id FROM albums WHERE name = '" + albumName + "');";
	make_the_sql(my_command, 0);

	my_command = "DELETE FROM albums WHERE name = '" + albumName + "' and user_id = " + std::to_string(userId) + ";";	
	make_the_sql(my_command, 0);
}


void DatabaseAccess::tagUserInPicture(const std::string& albumName, const std::string& pictureName, int userId)
{
	std::string my_command = "INSERT INTO tags (picture_id, user_id) VALUES ((SELECT id FROM pictures WHERE name = '" + pictureName + "' AND album_id = (SELECT id FROM albums WHERE name = '" + albumName + "')), " + std::to_string(userId) + ");";
	make_the_sql(my_command, 0);
}

void DatabaseAccess::untagUserInPicture(const std::string& albumName, const std::string& pictureName, int userId)
{
	std::string my_command = "DELETE FROM tags WHERE picture_id = (SELECT id FROM pictures WHERE name = '" + pictureName + "' AND album_id = (SELECT id FROM albums WHERE name = '" + albumName + "') and user_id = " + std::to_string(userId) + ");";
	make_the_sql(my_command, 0);
}

void DatabaseAccess::createUser(User& user)
{
	std::string my_command = "INSERT INTO users (name) VALUES ('" + user.getName() + "');";
	
	make_the_sql(my_command, 0);
}

void DatabaseAccess::deleteUser(const User& user)
{
	std::string my_command = "DELETE FROM users WHERE id = " + std::to_string(user.getId()) + ";";
	
	make_the_sql(my_command, 0);
}

const std::list<Album> DatabaseAccess::getAlbums()
{
	make_the_sql("SELECT * FROM albums;", 1);	
	return get_albums_from_result();
}

const std::list<Album> DatabaseAccess::getAlbumsOfUser(const User& user)
{
	make_the_sql("SELECT * FROM albums WHERE user_id =  " + std::to_string(user.getId()) + ";", 1);
	return get_albums_from_result();
}

void DatabaseAccess::createAlbum(const Album& album)
{
	make_the_sql("INSERT INTO albums (name, creation_date, user_id) VALUES ('" + album.getName() + "', '" + album.getCreationDate() +  "', "  + std::to_string(album.getOwnerId()) + ");", 0);
}

bool DatabaseAccess::doesAlbumExists(const std::string& albumName, int userId)
{
	make_the_sql("SELECT * FROM albums WHERE user_id =  " + std::to_string(userId) + " and name = '" + albumName + "';", 1);
	return DatabaseAccess::result.length() != 0;
}


Album DatabaseAccess::openAlbum(const std::string& albumName)
{
	make_the_sql("SELECT * FROM albums WHERE name = '" + albumName + "';", 1);
	Album open_album = get_albums_from_result().front();

	make_the_sql("SELECT * FROM pictures WHERE album_id = (SELECT id FROM albums WHERE name = '" + albumName + "');", 1);
	auto pictures = get_pictures_from_result();
	
	for (auto it = pictures.begin(); it != pictures.end(); it++)
	{
		make_the_sql("SELECT user_id FROM tags WHERE picture_id = " + std::to_string(it->getId()) + ";", 1);
		get_users_tag_from_result(*it);
		open_album.addPicture(*it);
	}
	return open_album;
}

void DatabaseAccess::printAlbums()
{
	auto album_list = this->getAlbums();
	for (auto it = album_list.begin(); it != album_list.end(); it++)
	{
		std::cout << *it << std::endl;
	}
}

void DatabaseAccess::addPictureToAlbumByName(const std::string& albumName, const Picture& picture)
{
	make_the_sql("INSERT INTO pictures (name, creation_date, album_id, location) VALUES ('" + picture.getName() + "', '" + picture.getCreationDate() + "', (SELECT id FROM albums WHERE name = '" + albumName + "'), '" + picture.getPath() + "');", 0);
}

void DatabaseAccess::removePictureFromAlbumByName(const std::string& albumName, const std::string& pictureName)
{
	make_the_sql("DELETE FROM pictures WHERE name = '" + pictureName + "' and album_id = (SELECT id FROM albums WHERE name = '" + albumName + "');", 0);
}

void DatabaseAccess::printUsers()
{
	make_the_sql("SELECT * FROM users;", 1);
	auto user_list = get_users_from_result();
	for (auto it = user_list.begin(); it != user_list.end(); it++)
	{
		std::cout << *it << std::endl;
	}
}

User DatabaseAccess::getUser(int userId)
{
	make_the_sql("SELECT * FROM users WHERE id =  " + std::to_string(userId) + ";", 1);
	return get_users_from_result().front();
}

bool DatabaseAccess::doesUserExists(int userId)
{
	make_the_sql("SELECT * FROM users WHERE id = '" + std::to_string(userId) + "';", 1);
	return DatabaseAccess::result.length() != 0;
}

int DatabaseAccess::countAlbumsOwnedOfUser(const User& user)
{
	make_the_sql("SELECT * FROM albums WHERE user_id =  " + std::to_string(user.getId()) + ";", 1);
	get_albums_from_result();
	return DatabaseAccess::amount;
}

int DatabaseAccess::countAlbumsTaggedOfUser(const User& user)
{
	auto album_list = this->getAlbums();
	int count = 0;

	for (auto it = album_list.begin(); it != album_list.end(); it++)
	{
		make_the_sql("SELECT * FROM pictures WHERE album_id = (SELECT id FROM albums WHERE name = '" + it->getName() + "');", 1);
		auto pictures = get_pictures_from_result();
		for (auto it2 = pictures.begin(); it2 != pictures.end(); it2++)
		{
			make_the_sql("SELECT user_id FROM tags WHERE picture_id = " + std::to_string(it2->getId()) + ";", 1);
			get_users_tag_from_result(*it2);
			if (it2->getUserTags().find(user.getId()) != it2->getUserTags().end())
			{
				count++;
				break;
			}
		}
	}
	
	return count;
}

int DatabaseAccess::countTagsOfUser(const User& user)
{
	make_the_sql("SELECT user_id FROM tags WHERE user_id = " + std::to_string(user.getId()) + ";", 1);
	get_users_tag_from_result(Picture(10, "blabla"));
	return DatabaseAccess::amount;
}

float DatabaseAccess::averageTagsPerAlbumOfUser(const User& user)
{
	int albums_amoutn = getAlbumsOfUser(user).size();
	int tags_amount = countTagsOfUser(user);
	return (float)tags_amount / albums_amoutn;
}

User DatabaseAccess::getTopTaggedUser()
{
	make_the_sql("SELECT * FROM users;", 1);
	auto user_list = get_users_from_result();
	int max = 0, num = 0;
	User* max_taged = nullptr;

	for (auto it = user_list.begin(); it != user_list.end(); it++)
	{
		num = countTagsOfUser(*it);
		if (num > max)
		{
			max = num;
			max_taged = &(*it);
		}
	}

	return *max_taged;
}

Picture DatabaseAccess::getTopTaggedPicture()
{
	make_the_sql("SELECT * FROM pictures WHERE id in (SELECT picture_id FROM tags);", 1);
	auto pictures = get_pictures_from_result();
	
	Picture* Pmax_tags_pic = nullptr;
	int max_tags = 0, num;
	
	for (auto it = pictures.begin(); it != pictures.end(); it++)
	{
		make_the_sql("SELECT user_id FROM tags WHERE picture_id = " + std::to_string(it->getId()) + ";", 1);
		get_users_tag_from_result(*it);
		
		num = DatabaseAccess::amount;
		if (num > max_tags)
		{
			Pmax_tags_pic = &(*it);
			max_tags = num;
		}
	}
	
	return *Pmax_tags_pic;
}

std::list<Picture> DatabaseAccess::getTaggedPicturesOfUser(const User& user)
{
	make_the_sql("SELECT * FROM pictures WHERE id in (SELECT picture_id FROM tags WHERE user_id = " + std::to_string(user.getId()) + ");", 1);
	return get_pictures_from_result();
}

int DatabaseAccess::callback(void* data, int argc, char** argv, char** azColName)
{
	for (int i = 0; i < argc; i++)
	{
		DatabaseAccess::result += ",";
		DatabaseAccess::result += argv[i];
	}
	DatabaseAccess::result += "&";

	return 0;
}

void DatabaseAccess::make_the_sql(std::string command, bool choice)
{
	DatabaseAccess::result = "";
	char* error;
	int (*ptrFunc)(void* data, int argc, char** argv, char** azColName);
	ptrFunc = DatabaseAccess::callback;

	if (choice)
	{
		int res = sqlite3_exec(data_base_pointer, command.c_str(), ptrFunc, nullptr, &error);
	}
	else
	{
		int res = sqlite3_exec(data_base_pointer, command.c_str(), nullptr, nullptr, &error);
	}
}

std::list<Album> DatabaseAccess::get_albums_from_result()
{

	std::list<Album> albums;
	int album_num = 0, id = 0, user_id = 0, num_col = 0;
	std::string temp = DatabaseAccess::result, name = "", creation_date = "";
	DatabaseAccess::amount = 0;

	for (int i = 0; i < temp.length(); i++)
	{

		if (temp[i] == ',')
		{
			std::string value = "";
			for (int j = i + 1; temp[j] != ',' && temp[j] != '&'; j++)
			{
				value += temp[j];
				i = j;
			}
			if (num_col == 0)
				id = stoi(value);
			if (num_col == 1)
				name = value;
			if (num_col == 2)
				creation_date = value;
			if (num_col == 3)
				user_id = stoi(value);
			num_col = (num_col + 1) % 4;
		}
		if (temp[i] == '&')
		{
			album_num++;
			albums.push_back(Album(user_id, name, creation_date));
		}	
	}

	DatabaseAccess::amount = album_num;
	return albums;
}

std::list<User> DatabaseAccess::get_users_from_result()
{
	DatabaseAccess::amount = 0;

	std::list<User> users;
	int users_num = 0, id = 0, num_col = 0;
	std::string temp = DatabaseAccess::result, name = "";

	for (int i = 0; i < temp.length(); i++)
	{
		if (temp[i] == ',')
		{
			std::string value = "";
			for (int j = i + 1; temp[j] != ',' && temp[j] != '&'; j++)
			{
				value += temp[j];
				i = j;
			}
			if (num_col == 0)
				id = stoi(value);
			if (num_col == 1)
				name = value;
			num_col = (num_col + 1) % 2;
		}
		if (temp[i] == '&')
		{
			users_num++;
			users.push_back(User(id, name));
		}
	
	}

	DatabaseAccess::amount = users_num;
	return users;
}

std::list<Picture> DatabaseAccess::get_pictures_from_result()
{
	std::list<Picture> pictures;
	int pictures_num = 0, id = 0, num_col = 0, album_id = 0;
	std::string temp = DatabaseAccess::result, name = "", creation_date = "", location = "";
	DatabaseAccess::amount = 0;

	for (int i = 0; i < temp.length(); i++)
	{
		if (temp[i] == ',')
		{	
			std::string value = "";
			for (int j = i + 1; temp[j] != ',' && temp[j] != '&'; j++)
			{
				value += temp[j];
				i = j;
			}
			if (num_col == 0)
				id = stoi(value);
			if (num_col == 1)
				name = value;
			if (num_col == 2)
				creation_date = value;
			if (num_col == 3)
				album_id = stoi(value);
			if (num_col == 4)
				location = value;
			
			num_col = (num_col + 1) % 5;
		}
		if (temp[i] == '&')
		{
			pictures_num++;
			pictures.push_back(Picture(id, name, location, creation_date));
		}

	}

	DatabaseAccess::amount = pictures_num;
	return pictures;
}

void DatabaseAccess::get_users_tag_from_result(Picture& picture)
{
	DatabaseAccess::amount = 0;
	std::string temp = DatabaseAccess::result;
	for (int i = 0; i < temp.length(); i++)
	{
		if (temp[i] != ',' && temp[i] != '&')
		{
			DatabaseAccess::amount++;
			int id = (int)temp[i] - 48;
			picture.tagUser(id);
		}
	}
}
