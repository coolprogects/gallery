#pragma once
#include "IDataAccess.h"
#include "sqlite3.h"
#include "io.h"

class DatabaseAccess : public IDataAccess
{

public:

	static std::string result;
	static int amount;

	DatabaseAccess() = default;
	~DatabaseAccess() = default;

	//EX1 related.
	virtual bool open();
	virtual void close();
	virtual void clear();
	virtual void closeAlbum(Album& pAlbum);

	//EX2 related.
	virtual void deleteAlbum(const std::string& albumName, int userId);
	virtual void tagUserInPicture(const std::string& albumName, const std::string& pictureName, int userId);
	virtual void untagUserInPicture(const std::string& albumName, const std::string& pictureName, int userId);
	virtual void createUser(User& user);
	virtual void deleteUser(const User& user);

	//EX3 related.
	virtual void printUsers();
	virtual User getUser(int userId);
	virtual bool doesUserExists(int userId);
	virtual const std::list<Album> getAlbums();
	virtual const std::list<Album> getAlbumsOfUser(const User& user);
	virtual void createAlbum(const Album& album);
	virtual bool doesAlbumExists(const std::string& albumName, int userId);
	virtual void printAlbums();
	virtual Album openAlbum(const std::string& albumName);
	virtual void addPictureToAlbumByName(const std::string& albumName, const Picture& picture);
	virtual void removePictureFromAlbumByName(const std::string& albumName, const std::string& pictureName);
	
	
	virtual int countAlbumsOwnedOfUser(const User& user);
	virtual int countAlbumsTaggedOfUser(const User& user);
	virtual int countTagsOfUser(const User& user);
	virtual float averageTagsPerAlbumOfUser(const User& user);

	// queries
	virtual User getTopTaggedUser();
	virtual Picture getTopTaggedPicture();
	virtual std::list<Picture> getTaggedPicturesOfUser(const User& user);

	
	//Utils function.
	void make_the_sql(std::string command, bool choice);
	std::list<Album> get_albums_from_result();
	std::list<User> get_users_from_result();
	std::list<Picture> get_pictures_from_result();
	void get_users_tag_from_result(Picture& picture);
	
	//callback function.
	static int callback(void* data, int argc, char** argv, char** azColName);

private:
	sqlite3* data_base_pointer;
};

	